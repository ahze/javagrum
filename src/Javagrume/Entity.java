package Javagrume;


import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

public abstract class Entity implements Tabable{

    //<editor-fold attributes/constructor>
    protected HashMap<String, Object> attributes;
    private boolean isNew;

    public Entity() {
        attributes = new HashMap<>();
        isNew = true;
    }
    //</editor-fold>

    //<editor-fold getterToImplement>
    public abstract String getTableName();
    //</editor-fold>




    //<editor-fold database>

    /**
     * Saves the object in the database
     *
     * @throws JavAgrumException
     */
    public final void save() throws JavAgrumException{
        if(this.isNew)
            this.saveNew();
        else
            this.update();
    }

    /**
     * insert the object in the database
     * @throws JavAgrumException
     */
    private void saveNew() throws JavAgrumException{
        String query = "INSERT INTO "+this.getTableName()+"(";
        List<Column> columnsList = getColumnsWithCorrectValue();
        for (int i = 0; i < columnsList.size(); i++) {
            Column column = columnsList.get(i);
            query += column.getColumnName() + (i == columnsList.size() -1 ? "" :", ");
        }
        query += ") VALUES(";
        for(int i = 0; i < columnsList.size(); i++){
            query += "?"+ (i == columnsList.size() -1 ? "" :", ");
        }
        query += ")";
        PreparedStatement st = DBConnection.prepareStatement(query);
        fillStatement(st, columnsList);
        try {
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        this.isNew = false;

        Column auto_incremented = getColumsAsList().where(Column::isAutoIncrement).first();
        if(auto_incremented != null){
            try(ResultSet generatedKey = st.getGeneratedKeys()){
                if(generatedKey.next())
                    this.set(auto_incremented.getColumnName(), generatedKey.getInt(1));
            }
            catch (SQLException e){
                throw new JavAgrumException("Did not manage to get auto generater key\n"+e.getMessage());
            }
        }
    }


    /**
     * Update the line corresponding to the object
     * @throws JavAgrumException
     */
    private void update() throws JavAgrumException{
        String query = "UPDATE "+this.getTableName()+" SET";
        List<Column> columnsList = getColumnsWithCorrectValue();
        for (int i = 0; i < columnsList.size(); i++) {
            Column column = columnsList.get(i);
            query += " " + column.getColumnName() + " = ?" + (i == columnsList.size() -1 ? "" : ", ");
        }
        query += " WHERE ";
        JavAgrumList<Column> primaryKeys = this.getPrimaryKeys();
        for(int i = 0; i < primaryKeys.size(); i++){
            query += (i > 0 ? " AND " : "")+getPrimaryKeys().get(i).getColumnName()+" = ? ";
        }
        PreparedStatement st = DBConnection.prepareStatement(query);
        this.fillStatement(st, columnsList);
        for(int i = 0; i < primaryKeys.size(); i++){
            this.setValueInStatement(st, primaryKeys.get(i), i+1+columnsList.size());
        }
        try {
            st.executeUpdate();
        } catch (SQLException e) {
            throw new JavAgrumException("Did not manage to update "+this.getTableName()+" row");
        }
    }

    /**
     *
     * @throws JavAgrumException
     */
    public final void delete() throws JavAgrumException {
        delete(false);
    }


    /**
     * Delete the corresponding line of the object on the database
     * If the param mustBeNew is true and the object is new, the function throws an exception
     * @param mustNotBeNew indicates if the object must be in the database
     * @throws JavAgrumException
     */
    public void delete(boolean mustNotBeNew) throws JavAgrumException{
        try {
            if (this.isNew){
                if(mustNotBeNew)
                    throw new JavAgrumException("Trying to delete a non recorded object");
                else
                    return;
            }
            String query = "DELETE FROM " + this.getTableName() + " WHERE ";
            JavAgrumList<Column> primaryKeys = this.getPrimaryKeys();
            for (int i = 0; i < primaryKeys.size(); i++) {
                query += (i > 0 ? " AND " : "") + getPrimaryKeys().get(i).getColumnName() + " = ? ";
            }
            PreparedStatement st = DBConnection.prepareStatement(query);
            this.fillStatement(st, this.getPrimaryKeys());
            st.executeUpdate();
        }
        catch (SQLException e){
            throw new JavAgrumException("Did not manage to delete "+this.getTableName()+" row");
        }
    }
    //</editor-fold>

    //<editor-fold table links>

    /**
     *
     * @param tClass
     * @param data foreignKey bindingKey
     * @throws JavAgrumException
     */
    public final <T extends Entity> void hasMany(Class<T> tClass, String... data) throws JavAgrumException{
        has(tClass, true, data);
    }

    public final <T extends Entity> void hasOne(Class<T> tClass, String... data) throws JavAgrumException{
        has(tClass, false, data);
    }

    private <T extends Entity> void has(Class<T> tClass, boolean many, String... data) throws JavAgrumException {
        String atrName = "",
                foreignKey = "",
                bindingKey = "";
        switch (data.length) {
            case 2:
                foreignKey = bindingKey = data[1];
            case 1:
                atrName = data[0];
            case 0:
                final String linkTabName = newInstance(tClass).getTableName();
                final String tabName = this.getTableName();
                Column ref = this.getColumsAsList().where(c -> c.isForeignKey() && c.getReferencedColumn().getTableName().equals(linkTabName)).first();
                if(data.length != 0) break;;
                atrName = linkTabName + (many ? "s" : "");
                if (ref == null) {
                    ref = newInstance(tClass).getColumsAsList().where(c -> c.isForeignKey() && c.getReferencedColumn().getTableName().equals(tabName)).first();
                    if (ref == null) {
                        throw new JavAgrumException("Unable to find a link beetween " + this.getTableName() + " and " + linkTabName);
                    }
                    foreignKey = ref.getColumnName();
                    bindingKey = ref.getReferencedColumn().getColumnName();
                } else {
                    foreignKey = ref.getReferencedColumn().getColumnName();
                    bindingKey = ref.getColumnName();
                }

                break;
            case 3:
                atrName = data[0];
                foreignKey = data[1];
                foreignKey = data[2];


        }

        if(many)
            this.set(atrName, where(tClass, foreignKey+" = "+this.get(bindingKey).toString()));
        else
            this.set(atrName, where(tClass, foreignKey+" = "+this.get(bindingKey).toString()).first());
    }
    //</editor-fold>



    //<editor-fold getters>
    final JavAgrumList<Column> getPrimaryKeys(){
        return this.getColumsAsList().where(Column::isPrimaryKey);
    }

    public final int getInt(String attrName){
        try{
            return (int)attributes.get(attrName);
        }
        catch (ClassCastException e){
            return 0;
        }
    }
    public final String getString(String attrName){
        try{
            return (String)attributes.get(attrName);
        }
        catch (Exception e){
            return null;
        }
    }
    public final float getFloat(String attrName){
        try{
            return (float)attributes.get(attrName);
        }
        catch (Exception e){
            return 0f;
        }
    }
    public final Date getDate(String attrName){
        try{
            return (Date)attributes.get(attrName);
        }
        catch (Exception e){
            return null;
        }
    }
    public final Object get(Object o){
        return attributes.get(o);
    }
    public final HashMap<String, Object> getAttributes() {
        return attributes;
    }
    public String toString(){
        return this.toString(0);
    }

    public String toString(int tab){
        String inner_tabs = tabs(tab+1);
        String res = tabs(tab);
        res += this.getClass().getSimpleName()+"{\n";
        for(String key : attributes.keySet()){
            Object value = this.attributes.getOrDefault(key, "null");
            String str;
            if(value instanceof Tabable) str = "\n"+((Tabable) value).toString(tab+2);
            else str = value.toString();
            res += inner_tabs+key+" : "+str+"\n";
        }
        return res+tabs(tab)+"}";
    }
    //</editor-fold>


    //<editor-fold setters>
    public final void set(String attrName, Object value) {
        Column c = this.getColumsAsList().where(cc -> cc.getColumnName().equals(attrName)).first();
        if(c != null)
            if(!c.getJavaType().equals(value.getClass().getSimpleName()))
                throw new IllegalArgumentException("Tried to fill the field "+attrName+" typed "+c.getDataType()+"/"+c.getJavaType()+" with a value typed "+value.getClass().getSimpleName());
        this.attributes.put(attrName, value);
    }
    //</editor-fold>




    //<editor-fold static>
    public static <T extends Entity> JavAgrumList<T> where(Class<T> tClass, Object... options) throws JavAgrumException{
        String query = "SELECT * \nFROM "+newInstance(tClass).getTableName();
        if(options.length > 0){
            query += " \nWHERE "+options[0];
        }
        for(int i = 1; i < options.length; i++){
            query += " \nAND "+options[i];
        }
        PreparedStatement st = DBConnection.prepareStatement(query);
        ResultSet rs = null;
        try {
            rs = st.executeQuery();
        } catch (SQLException e) {
            throw new JavAgrumException(st.toString());
        }
        JavAgrumList<T> res = new JavAgrumList<>();

        try {
            while (rs.next()) {
                res.add(putAttributesInEntityAndReturnIt(newInstance(tClass), rs));
            }
        }
        catch (SQLException e){
            throw new JavAgrumException(e.getMessage());
        }
        return res;
    }

    /**
     * @param tClass The class of the corresponding table
     * @return all entities of the class in parameter
     */
    public static <T extends Entity> JavAgrumList<T> all(Class<T> tClass) throws JavAgrumException{
        return where(tClass);
    }


    public static <T extends Entity> T find(Class<T> tClass, Object... keys) throws JavAgrumException{
        try {
            T newEntity = newInstance(tClass);
            if (newEntity.getPrimaryKeys().size() != keys.length) {
                throw new JavAgrumException("Keys number does not match : " + keys.length + " instead of " + newEntity.getPrimaryKeys().size());
            }

            String query = "SELECT * FROM " + newEntity.getTableName();
            for (int i = 0; i < newEntity.getPrimaryKeys().size(); i++) {
                query += (i == 0 ? " WHERE " : " AND ") + newEntity.getPrimaryKeys().get(i).getColumnName() + " = ? ";
            }
            PreparedStatement st = DBConnection.prepareStatement(query);
            for (int i = 0; i < keys.length; i++) {
                st.setString(i + 1, keys[i].toString());
            }
            ResultSet rs = st.executeQuery();
            rs.next();
            T res = putAttributesInEntityAndReturnIt(newEntity, rs);
            ((Entity) res).isNew = false;
            return res;
        }
        catch (SQLException e) {
            throw new JavAgrumException(e.getMessage());
        }
    }

    private static <T extends Entity> T newInstance(Class<T> tClass) throws JavAgrumException {
        try{
            return tClass.newInstance();
        }
        catch (IllegalAccessException | InstantiationException e) {
            throw new JavAgrumException("Can not create instant of "+tClass.getSimpleName()+" class (public default constructor may miss)");
        }
    }


    private static <T extends Entity> T putAttributesInEntityAndReturnIt(T t, ResultSet rs) throws JavAgrumException{
        try {
            for (Column c : Column.getTableColumns(t.getTableName()).values()) {
                Object toAdd;
                String column_name = c.getColumnName();
                switch (c.getDataType().toLowerCase()) {
                    case "int":
                    case "bit":
                    case "tinyint":
                    case "smallint":
                        toAdd = rs.getInt(column_name);
                        break;
                    case "bigint":
                        toAdd = rs.getLong(column_name);
                        break;
                    case "decimal":
                    case "float":
                        toAdd = rs.getFloat(column_name);
                        break;
                    case "date":
                    case "datetime":
                    case "time":
                    case "timestamp":
                        toAdd = rs.getDate(column_name);
                        break;
                    case "varchar":
                    case "char":
                    case "text":
                    default:
                        toAdd = rs.getString(column_name);
                        break;
                }
                t.getAttributes().put(column_name, toAdd);
            }
            return t;
        }
        catch (SQLException e){
            throw new JavAgrumException(e.getMessage());
        }
    }
    //</editor-fold>

    //<editor-fold tools>
    private void setValueInStatement(PreparedStatement st, Column column, int i) throws JavAgrumException {
        try {
            switch (column.getDataType().toLowerCase()) {
                case "int":
                case "bit":
                case "tinyint":
                case "smallint":
                    st.setInt(i, (int) this.attributes.get(column.getColumnName()));
                    break;
                case "bigint":
                    st.setLong(i, (long) this.attributes.get(column.getColumnName()));
                    break;
                case "decimal":
                case "float":
                    st.setFloat(i, (float) this.attributes.get(column.getColumnName()));
                    break;
                case "date":
                case "datetime":
                case "time":
                case "timestamp":
                    st.setDate(i, (Date) this.attributes.get(column.getColumnName()));
                    break;
                case "varchar":
                case "char":
                case "text":
                default:
                    st.setString(i, (String) this.attributes.get(column.getColumnName()));
                    break;
            }
        }
        catch (SQLException e){
            throw new JavAgrumException("Can not put "+this.get(column.getColumnName()).getClass().getSimpleName()+" in a "+column.getDataType()+"/"+column.getJavaType());
        }
    }

    private void fillStatement(PreparedStatement st, List<Column> columnsList) throws JavAgrumException{
        for(int i = 0; i < columnsList.size(); i++){
            Column column = columnsList.get(i);
            this.setValueInStatement(st, column, i+1);
        }
    }

    private void fillStatement(PreparedStatement st) throws JavAgrumException{
        List<Column> columnsList = this.getColumsAsList();
        fillStatement(st, columnsList);
    }

    JavAgrumList<Column> getColumsAsList(){
        return new JavAgrumList<>(Column.getTableColumns(this.getTableName()).values());
    }

    /**
     * Get the list of columns that have a correct value
     * @return
     * @throws JavAgrumException if a mandatory field is empty
     */
    private JavAgrumList<Column> getColumnsWithCorrectValue() throws JavAgrumException{
        JavAgrumList<Column> columnsList = this.getColumsAsList();
        for(int i = 0; i < columnsList.size(); i++){
            Column c = columnsList.get(i);
            if(this.attributes.get(c.getColumnName()) == null){
                if(c.isNullable() || c.isAutoIncrement()){
                    columnsList.remove(c);
                    i--;
                }
                else{
                    throw new JavAgrumException("Missing column : "+c.getColumnName());
                }
            }
        }
        return columnsList;
    }
    //</editor-fold>


}



