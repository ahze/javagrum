package Javagrume;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

class Column implements Tabable {

    private boolean isPrimaryKey, isNullable, is_auto_increment;
    private int ordinal_position;
    private String table_schema, table_name, column_name, data_type;

    private Column referencedColumn;

    public Column(String table_schema, String table_name, String column_name,
                  String data_type, boolean is_auto_increment, boolean isNullable,
                  int ordinal_position) {
        this.isPrimaryKey = false;
        this.isNullable = isNullable;
        this.table_schema = table_schema;
        this.table_name = table_name;
        this.column_name = column_name;
        this.data_type = data_type;
        this.is_auto_increment = is_auto_increment;
        this.ordinal_position = ordinal_position;
    }




    //<editor-fold getters>

    public boolean isPrimaryKey() {
        return isPrimaryKey;
    }

    public boolean isForeignKey(){
        return this.referencedColumn != null;
    }

    public boolean isNullable() {
        return isNullable;
    }

    public String getTableSchema() {
        return table_schema;
    }

    public String getTableName() {
        return table_name;
    }

    public String getColumnName() {
        return column_name;
    }

    public String getDataType() {
        return data_type;
    }

    public int getOrdinalPosition() {
        return ordinal_position;
    }

    public boolean isAutoIncrement() {
        return is_auto_increment;
    }

    String getJavaType(){
        switch(this.data_type.toLowerCase()){
            case "int":
            case "bit":
            case "tinyint":
            case "smallint":
                return "Integer";
            case "bigint":
                return "Long";
            case "decimal":
            case "float":
                return "Float";
            case "date":
            case "datetime":
            case "time":
            case "timestamp":
                return "Date";
            case "varchar":
            case "char":
            case "text":
            default:
                return "String";
        }
    }

    public Column getReferencedColumn() {
        return referencedColumn;
    }

    @Override
    public String toString() {
        return this.toString(0);
    }


    @Override
    public String toString(int tab) {
        String tabs = tabs(tab+1);
        return tabs(tab)+"Column "+table_name+"."+column_name+"{\n"+
                tabs+"data_type : "+data_type+"\n"+
                tabs+"is_auto_increment : "+is_auto_increment+"\n"+
                tabs+"isPrimaryKey : "+isPrimaryKey+"\n"+
                tabs+"isNullable : "+isNullable+"\n"+
                (isForeignKey() ?
                        tabs+"references : "+referencedColumn.table_name+"."+referencedColumn.column_name+"\n"
                        : "")+
                tabs(tab)+"}";
    }

    //</editor-fold>


    //<editor-fold static>
    static HashMap<String, HashMap<String, Column>> columns;

    static HashMap<String, Column> getTableColumns(String tableName){
        if(!columns.containsKey(tableName)){
            columns.put(tableName, new HashMap<>());
        }
        return columns.get(tableName);
    }


    static void initColumns(String table_schema) throws JavAgrumException {
        try {
            columns = new HashMap<>();
            String query = "SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = ?";
            PreparedStatement st = DBConnection.prepareStatement(query);
            st.setString(1, table_schema);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                String table_name = rs.getString("TABLE_NAME");
                String column_name = rs.getString("COLUMN_NAME");
                int ordinal_position = rs.getInt("ORDINAL_POSITION");
                boolean is_nullable = rs.getString("IS_NULLABLE").toUpperCase().equals("YES");
                String data_type = rs.getString("data_type");
                boolean is_auto_increment = rs.getString("EXTRA").contains("auto_increment");

                getTableColumns(table_name).put(column_name,
                        new Column(table_schema, table_name, column_name, data_type, is_auto_increment, is_nullable, ordinal_position));
            }

            query = "SELECT * FROM information_schema.KEY_COLUMN_USAGE WHERE TABLE_SCHEMA = ?;";
            st = DBConnection.prepareStatement(query);
            st.setString(1, table_schema);
            rs = st.executeQuery();
            while (rs.next()) {
                String table_name = rs.getString("TABLE_NAME");
                String column_name = rs.getString("COLUMN_NAME");
                Column referencing_column = getTableColumns(table_name).get(column_name);
                if (rs.getString("CONSTRAINT_NAME").toUpperCase().equals("PRIMARY")) {
                    referencing_column.isPrimaryKey = true;
                } else {
                    String referenced_table = rs.getString("REFERENCED_TABLE_NAME");
                    if (referenced_table != null && !referenced_table.equals("")) {
                        String referenced_column = rs.getString("REFERENCED_COLUMN_NAME");
                        referencing_column.referencedColumn = getTableColumns(referenced_table).get(referenced_column);
                    }

                }
            }
        }
        catch (SQLException e){
            throw new JavAgrumException(e.getMessage());
        }
    }
    //</editor-fold>

}
