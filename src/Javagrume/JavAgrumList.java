package Javagrume;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class JavAgrumList<T extends Tabable> extends ArrayList<T> implements Tabable{

    public JavAgrumList() {
        super();
    }

    public JavAgrumList(Collection<? extends T> c) {
        super(c);
    }

    public T first(){
        return this.size() > 0 ? this.get(0) : null;
    }

    public JavAgrumList<T> where(Predicate<T> predicate){
        return this.stream().filter(predicate).collect(Collectors.toCollection(JavAgrumList::new));
    }

    @Override
    public String toString() {
        return this.toString(0);
    }

    @Override
    public String toString(int tab) {
        String res = tabs(tab)+"[\n";
        int size = size();
        for(int i = 0; i < size; i++){
            res += get(i).toString(tab+1);
            if(i < size-1)
                res += ",\n";
        }
        return res+"\n"+tabs(tab)+"]";
    }
}
