package Javagrume;

public class JavAgrumException extends Exception {

    public JavAgrumException(String message) {
        super(message);
    }
}
