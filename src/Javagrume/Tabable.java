package Javagrume;

public interface Tabable {

    String toString(int tab);

    default String tabs(int tab){
        String res = "";
        for(int i = 0; i < tab; i++)
            res += "\t";
        return res;
    }

}
