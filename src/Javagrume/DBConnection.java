package Javagrume;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.HashMap;

public class DBConnection {

    private static Connection global_connection;

    public static void init() throws JavAgrumException {
        init("conf/conf.ini");
    }

    public  static void init(String fileName) throws JavAgrumException {
        try {
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            HashMap<String, String> map = new HashMap<>();
            String line;
            while ((line = br.readLine()) != null) {
                String[] s = line.split("=");
                map.put(s[0].toLowerCase(), s[1]);
            }
            init(map.get("host"), map.get("port"), map.getOrDefault("dbname", map.get("databasename")), map.get("user"), map.get("password"));
        }
        catch (FileNotFoundException e){
            throw new JavAgrumException("Can not open '"+fileName+"' content");
        } catch (IOException e) {
            throw new JavAgrumException("Can not read '"+fileName+"' content");
        }
    }

    public static void init(String host, String port, String dbname, String user, String password) throws JavAgrumException{

        try {
            Class.forName("com.mysql.jdbc.Driver");
            global_connection = DriverManager.getConnection(
                    "jdbc:mysql://"+host+":"+port+"/"+dbname, user, password);
        } catch (ClassNotFoundException e) {
            throw new JavAgrumException("Can't load JDBC driver");
        } catch (SQLException e) {
            throw new JavAgrumException("Did not manage to connect database");
        }


        Column.initColumns(dbname);
    }

    public static Connection getGlobal_connection() {
        return global_connection;
    }

    public static PreparedStatement prepareStatement(String sql) throws JavAgrumException {
        try {
            return global_connection.prepareStatement(sql);
        } catch (SQLException e) {
            throw new JavAgrumException(e.getMessage());
        }
    }
}


